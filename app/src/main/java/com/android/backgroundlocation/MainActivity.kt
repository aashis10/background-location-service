package com.android.backgroundlocation

import android.Manifest
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.backgroundlocation.databinding.ActivityMainBinding
import com.android.backgroundlocation.services.LocationService
import xyz.kumaraswamy.autostart.Autostart
import xyz.kumaraswamy.autostart.Autostart.getSafeState


class MainActivity : AppCompatActivity() {

    private var service: Intent? = null
    private lateinit var binding: ActivityMainBinding
    private var permissionId = 3888
    var bound = false
    private lateinit var mService: LocationService

    private val backgroundLocation =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {

            }
        }

    private val locationPermissions =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            when {
                it.getOrDefault(Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        if (ActivityCompat.checkSelfPermission(
                                this, Manifest.permission.ACCESS_BACKGROUND_LOCATION
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            backgroundLocation.launch(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                        }
                    }
                }

                it.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {

                }
            }
        }

    private val mServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {

            Log.d("location", "serviceConnection")
            val binder = service as LocationService.LocationServiceBinder
            mService = binder.getService()
            mService.fetchLocation()
            bound = true
            Toast.makeText(this@MainActivity, "On Service Connection", Toast.LENGTH_SHORT).show()
        }

        override fun onServiceDisconnected(name: ComponentName) {
            bound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setup()
    }

    private fun setup() {
        checkXiaomi()
        initLocationService()
        initListener()
    }

    private fun checkXiaomi() {
        val manufacturer = "xiaomi"
        if (manufacturer.equals(Build.MANUFACTURER, ignoreCase = true)) {
            val enabled = getSafeState(this)
            Log.d("checkBackground", enabled.toString())
            if (!enabled) {
                Toast.makeText(
                    this@MainActivity,
                    "Enable background autostart to run services in background",
                    Toast.LENGTH_SHORT
                )
                    .show()
                val intent1 = Intent()
                intent1.component = ComponentName(
                    "com.miui.securitycenter",
                    "com.miui.permcenter.autostart.AutoStartManagementActivity"
                )
                startActivity(intent1)
            }
        }
    }

    private fun initLocationService() {
        service = Intent(this, LocationService::class.java)
    }

    private fun initListener() {
        binding.apply {
            btnStartBackGroundLocation.setOnClickListener { checkPermissions() }
            btnStopBackGroundLocation.setOnClickListener {
                stopService(service)
                Toast.makeText(this@MainActivity, "Background Service Stopped", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (ActivityCompat.checkSelfPermission(
                    this, Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                    this, Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                locationPermissions.launch(
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                )
            } else {
                askNotificationPermission()
            }
        }
    }

    private fun askNotificationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.POST_NOTIFICATIONS
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                bindService(
                    Intent(this, LocationService::class.java),
                    mServiceConnection,
                    BIND_AUTO_CREATE
                )
            } else if (shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS)) {
                val alertDialog =
                    AlertDialog.Builder(this).setTitle("Notification Permission Needed")
                        .setMessage("This app needs the Notification  permission, please accept to receive location notifications.")
                        .setNegativeButton("Cancel") { dialogInterface, i ->
                            bindService(
                                Intent(this, LocationService::class.java),
                                mServiceConnection,
                                BIND_AUTO_CREATE
                            )
                        }.setPositiveButton("OK") { dialogInterface, i ->
                            ActivityCompat.requestPermissions(
                                this, arrayOf(
                                    Manifest.permission.POST_NOTIFICATIONS
                                ), permissionId
                            )
                        }.create()
                alertDialog.setCancelable(false)
                alertDialog.show()
            } else {
                ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.POST_NOTIFICATIONS), permissionId
                )
            }
        } else {
            bindService(
                Intent(this, LocationService::class.java), mServiceConnection, BIND_AUTO_CREATE
            )
        }
    }
}